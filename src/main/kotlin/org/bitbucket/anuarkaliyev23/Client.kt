package org.bitbucket.anuarkaliyev23

import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.Socket

fun main() = runBlocking{
    val sockets = ArrayList<Socket>()
    val jobList = ArrayList<Job>()
    repeat(10) {
        val socket = Socket("127.0.0.1", 9000)
        val input = BufferedReader(InputStreamReader(socket.getInputStream()))
        input.lines().use { stream ->
            stream.forEach {
                println(it)
            }
        }
    }
}