package org.bitbucket.anuarkaliyev23

import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.IOException
import java.io.PrintWriter
import java.lang.Exception
import java.net.ServerSocket
import java.net.Socket
import java.time.ZonedDateTime
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor

fun main() = runBlocking<Unit> {
    val serverSocket = ServerSocket(9000)
    println("Server Socket launched and ready")
    try {
        var client : Socket? = null
        while (true) {
            try {
                client = serverSocket.accept()
                processClient(client)
            } catch (e : IOException) {
                System.out.println("I/O error: $e")
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        serverSocket.close()
    }
}

suspend fun processClient(socket: Socket?) {
    socket?.use { client ->
        println("Client has requested us date! Let's send it")
        val out = PrintWriter(client.getOutputStream(), true)
        delay(1000)
        out.println(ZonedDateTime.now().toString())
        println("Sent Date to client. Good Job!")
    }
}